package kaddy.server

import kaddy.cache.CompressCache
import kaddy.cache.EtagCache
import kaddy.config.Config
import kaddy.config.ConfigUnitStatus
import kaddy.http.Request
import kaddy.http.ResponseStatus
import kaddy.http.response.*
import kaddy.kio.ReadBuffer
import kaddy.kio.WriteBuffer
import kotlinx.coroutines.experimental.TimeoutCancellationException
import kotlinx.coroutines.experimental.async
import kotlinx.coroutines.experimental.nio.aAccept
import kotlinx.coroutines.experimental.nio.aConnect
import kotlinx.coroutines.experimental.withTimeout
import java.io.File
import java.io.IOException
import java.net.InetSocketAddress
import java.net.StandardSocketOptions
import java.nio.channels.AsynchronousServerSocketChannel
import java.nio.channels.AsynchronousSocketChannel
import java.util.concurrent.TimeUnit

class ServerUnit(
        port: Int,
        private val configUnits: ArrayList<Config.Companion.ConfigUnit>,
        private val compressCache: CompressCache
) {

    private val bindAddress = InetSocketAddress(port)

    private val serverSocketChannel = AsynchronousServerSocketChannel.open().bind(bindAddress)!!

    suspend fun start() {
        serverSocketChannel.setOption(StandardSocketOptions.SO_REUSEADDR, true)
        serverSocketChannel.setOption(StandardSocketOptions.SO_REUSEPORT, true)
        while (true) {
            val conn = serverSocketChannel.aAccept()

            async { handle(conn) }
        }
    }

    private suspend fun handle(conn: AsynchronousSocketChannel) {
        conn.setOption(StandardSocketOptions.SO_KEEPALIVE, true)
        val readBuffer = ReadBuffer(conn)

        try {
            loop@ while (true) {
                val request = withTimeout(5 * 60, TimeUnit.SECONDS) { Request.buildRequest(readBuffer) }
                readBuffer.clear()

                var configUnit: Config.Companion.ConfigUnit? = null

                for (unit in configUnits) {
                    if (request.host == unit.host) {
                        configUnit = unit
                        break
                    }
                }

                if (configUnit == null) {
                    NotFoundResponse(conn).write()
                    continue@loop
                }

                val path = request.path.removePrefix("/").let {
                    if (it == "") return@let configUnit.index
                    else return@let it
                }

                if (path.contains("..")) {
                    NotFoundResponse(conn).write()
                    continue@loop
                }

                if (configUnit.status == ConfigUnitStatus.PROXY) {
                    try {
                        proxyHandle(request, conn, configUnit)
                        break@loop
                    } catch (e: IOException) {
                        conn.close()
                        break@loop
                    }
                }

                val response = buildResponse(configUnit, request, path, conn)

                response.write()

                if (response is InternalServerErrorResponse) {
                    conn.close()
                    break@loop
                }
            }
        } catch (e: TimeoutCancellationException) {
            conn.close()
        } catch (e: IOException) {
            conn.close()
        }
    }

    private fun buildResponse(
            configUnit: Config.Companion.ConfigUnit,
            request: Request,
            path: String,
            conn: AsynchronousSocketChannel
    ): Response {

        when (configUnit.status) {
            ConfigUnitStatus.OK -> {
                val file = File(configUnit.root, path)

                return when {
                    !file.exists() -> NotFoundResponse(conn)

                    EtagCache.getCache(request.ifNoneMatch) == EtagCache.EtagStatus.NEW -> {
                        NotModifiedResponse(conn, request.ifNoneMatch!!)
                    }

                    else -> {
                        if (request.gzip) OKResponse(conn, file, compressCache, request.ifNoneMatch)
                        else OKResponse(conn, file)
                    }
                }
            }

            ConfigUnitStatus.REDIRECT_301 -> {
                return when {
                    request.path == configUnit.redirectPath -> {
                        MovedResponse(conn, ResponseStatus.MOVED_PERMANENTLY, configUnit.redirectTarget)
                    }

                    else -> NotFoundResponse(conn)
                }
            }

            ConfigUnitStatus.REDIRECT_302 -> {
                return when {
                    request.path == configUnit.redirectPath -> {
                        MovedResponse(conn, ResponseStatus.FOUND, configUnit.redirectTarget)
                    }

                    else -> NotFoundResponse(conn)
                }
            }

            else -> return InternalServerErrorResponse(conn)
        }
    }

    private suspend fun proxyHandle(request: Request, conn: AsynchronousSocketChannel, configUnit: Config.Companion.ConfigUnit) {
        request.header.map.remove("Connection")
        request.header.map["X-Forwarded-For"] = request.remoteAddress.hostString

        request.header.map["Host"] = configUnit.proxyHost

        val requestByteArray = request.toByteArray()

        val proxySocketChannel = AsynchronousSocketChannel.open()

        try {
            proxySocketChannel.aConnect(configUnit.proxyAddress)
        } catch (e: IOException) {
            proxySocketChannel.close()
            throw e
        }

        val proxyWriteBuffer = WriteBuffer(proxySocketChannel, requestByteArray.size)
        val proxyReadBuffer = ReadBuffer(proxySocketChannel)

        try {
            proxyWriteBuffer.write(requestByteArray)
        } catch (e: IOException) {
            proxySocketChannel.close()
            throw e
        }

        ProxyResponse(conn, proxyReadBuffer).write()
    }
}