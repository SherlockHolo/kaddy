package kaddy.server

import kaddy.cache.CompressCache
import kaddy.config.Config
import kotlinx.coroutines.experimental.Deferred
import kotlinx.coroutines.experimental.async
import java.io.File

class Server(configFile: File) {
    private val config = Config.parseConfigFile(configFile)
    private val serverUnits = ArrayList<Deferred<Unit>>()
    private val compressCache = CompressCache()

    suspend fun start() {
        config.portSet.forEach {
            val serverUnit = ServerUnit(it, config.getConfigUnits(it)
                    ?: throw Throwable("read config error"), compressCache)

            val task = async { serverUnit.start() }
            serverUnits.add(task)
        }

        serverUnits.forEach {
            it.await()
        }
    }
}