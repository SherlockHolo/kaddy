package kaddy.kio

import kotlinx.coroutines.experimental.nio.aWrite
import java.nio.ByteBuffer
import java.nio.channels.AsynchronousSocketChannel

class WriteBuffer(
        private val channel: AsynchronousSocketChannel,
        capacity: Int = 81920,
        directByteBuffer: Boolean = false
) : Buffer() {
    var separator = "\n"

    private val innerBuffer =
            if (!directByteBuffer) {
                ByteBuffer.allocate(capacity)
            } else ByteBuffer.allocateDirect(capacity)

    override suspend fun read(dst: ByteArray): Int {
        throw IllegalAccessException("WriteBuffer can't read")
    }

    override suspend fun readLine(): String? {
        throw IllegalAccessException("WriteBuffer can't read")
    }

    override suspend fun write(byteArray: ByteArray, offset: Int, length: Int): Int {
        if (offset == 0 && length == byteArray.size) innerBuffer.put(byteArray)
        else innerBuffer.put(byteArray.copyOfRange(offset, offset + length))

        innerBuffer.flip()

        val written = channel.aWrite(innerBuffer)
        innerBuffer.clear()
        return written
    }

    suspend fun write(byteArray: ByteArray): Int {
        return write(byteArray, 0, byteArray.size)
    }

    override suspend fun writeLine(string: String): Int {
        return write((string + separator).toByteArray())
    }

    override fun clear() {
        innerBuffer.clear()
    }

    override fun close() {
        channel.close()
    }
}