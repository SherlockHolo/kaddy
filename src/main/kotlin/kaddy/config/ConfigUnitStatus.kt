package kaddy.config

enum class ConfigUnitStatus {
    OK, REDIRECT_301, REDIRECT_302, PROXY
}