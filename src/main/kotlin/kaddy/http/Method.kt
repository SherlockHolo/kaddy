package kaddy.http

enum class Method(val string: String) {
    GET("GET"),
    POST("POST")
}