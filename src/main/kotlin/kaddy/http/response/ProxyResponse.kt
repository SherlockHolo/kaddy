package kaddy.http.response

import kaddy.http.ResponseStatus
import kaddy.kio.ReadBuffer
import kaddy.kio.WriteBuffer
import java.io.IOException
import java.io.UnsupportedEncodingException
import java.nio.channels.AsynchronousSocketChannel

class ProxyResponse(
        conn: AsynchronousSocketChannel,
        private val proxyReadBuffer: ReadBuffer
) : Response() {

    private lateinit var innerResponseStatus: ResponseStatus
    override val responseStatus get() = innerResponseStatus

    private val writeBuffer = WriteBuffer(conn)

    init {
        header.map.clear()
        proxyReadBuffer.separator = "\r\n"
        writeBuffer.separator = "\r\n"
    }

    override suspend fun write() {
        try {
            var line = proxyReadBuffer.readLine() ?: throw IOException("read proxy http first header error")
            line.split(' ').apply {
                innerResponseStatus = when (this[1].toInt()) {
                    200 -> ResponseStatus.OK
                    301 -> ResponseStatus.MOVED_PERMANENTLY
                    302 -> ResponseStatus.FOUND
                    304 -> ResponseStatus.NOT_MODIFIED
                    404 -> ResponseStatus.NOT_FOUND
                    500 -> ResponseStatus.INTERNAL_SERVER_ERROR
                    else -> throw UnsupportedEncodingException("unsupported http opera status code")
                }
            }

            while (line != "") {
                line = proxyReadBuffer.readLine() ?: throw IOException("read proxy header error")

                val i = line.indexOf(": ")
                if (i != -1) header.map[line.substring(0 until i)] = line.substring(i + 2..line.lastIndex)
            }

            header.map.remove("Connection")

            writeBuffer.write(headerToByteArray())

            val chunked = header.map.contains("Transfer-Encoding")

            if (!chunked) {
                val length = header.map["Content-length"]?.toInt() ?: return

                val data = ByteArray(length)
                val read = proxyReadBuffer.read(data)

                writeBuffer.write(data, 0, read)
            } else {
                var length: Int

                while (true) {
                    val hexLength = proxyReadBuffer.readLine() ?: throw IOException("read proxy data error")
                    writeBuffer.writeLine(hexLength)

                    length = Integer.parseInt(hexLength, 16)

                    if (length == 0) break

                    var data: ByteArray

                    while (length > 0) {
                        data = if (length > 8192) ByteArray(8192)
                        else ByteArray(length)

                        val read = proxyReadBuffer.read(data)
                        writeBuffer.write(data, 0, read)

                        if (read != data.size) return
                        length -= data.size
                    }
                    proxyReadBuffer.readLine()
                    writeBuffer.write("\r\n".toByteArray())
                }
                writeBuffer.write("\r\n".toByteArray())
            }
        } catch (e: Throwable) {
            e.printStackTrace()
            throw e
        } finally {
            proxyReadBuffer.close()
            writeBuffer.close()
        }
    }
}