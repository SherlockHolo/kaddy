package kaddy.http.response

import kaddy.http.Header
import kaddy.http.ResponseStatus
import kaddy.http.Version

abstract class Response {
    val header = Header().apply {
        this.map["Connection"] = "keep-alive"
        this.map["Server"] = "kaddy/0.1"
    }

    open val version = Version.HTTP_1_1

    abstract val responseStatus: ResponseStatus

    abstract suspend fun write()

    open fun headerToByteArray(): ByteArray {
        val sb = StringBuilder()

        /** add http header **/

        sb.append(version.string)
        sb.append(' ')
        sb.append(responseStatus.codeAndStatus)
        sb.append("\r\n")

        header.map.forEach { key, value ->
            sb.append(key)
            sb.append(": ")
            sb.append(value)
            sb.append("\r\n")
        }

        sb.append("\r\n")

        return sb.toString().toByteArray()
    }

    var contentLength: Int
        set(value) {
            header.map["Content-Length"] = value.toString()
        }
        get() = header.map["Content-Length"]?.toInt() ?: -1
}