package kaddy.http.response

import kaddy.http.ResponseStatus
import kaddy.kio.FileBuffer
import kaddy.kio.WriteBuffer
import java.io.File
import java.io.IOException
import java.nio.channels.AsynchronousSocketChannel

class NotFoundResponse(conn: AsynchronousSocketChannel, private val notFoundHtml: File? = null) : Response() {
    override val responseStatus = ResponseStatus.NOT_FOUND
    private val writeBuffer = WriteBuffer(conn)

    init {
        header.map["Content-Type"] = "text/html; charset=utf-8"
    }

    override suspend fun write() {
        if (notFoundHtml != null) {
            val fileBuffer = try {
                FileBuffer(notFoundHtml)
            } catch (e: NoSuchFileException) {
                defaultWrite()
                return
            }

            contentLength = fileBuffer.size.toInt()

            writeBuffer.write(headerToByteArray())

            val byteArray = ByteArray(80 * 1024)

            try {
                while (true) {
                    val length = fileBuffer.read(byteArray)
                    writeBuffer.write(byteArray, 0, length)

                    if (length < byteArray.size) break
                }
            } catch (e: IOException) {
                writeBuffer.close()
            } finally {
                fileBuffer.close()
            }
        } else {
            defaultWrite()
        }
    }

    private suspend fun defaultWrite() {
        val info = "404 not found\r\n".toByteArray()
        header.map["Content-Length"] = info.size.toString()

        try {
            writeBuffer.write(headerToByteArray() + info)
        } catch (e: IOException) {
            writeBuffer.close()
        }
    }
}