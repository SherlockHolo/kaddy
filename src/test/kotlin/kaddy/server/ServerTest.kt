package kaddy.server

import kotlinx.coroutines.experimental.runBlocking
import java.io.File

fun main(args: Array<String>) = runBlocking {
    val server = Server(File(System.getProperty("user.dir") + "/src/main/kotlin/kaddy/config/example.conf"))
    server.start()
}