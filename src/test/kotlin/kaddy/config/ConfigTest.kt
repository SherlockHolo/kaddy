package kaddy.config

import org.junit.Test

import org.junit.Assert.*
import java.io.File

class ConfigTest {

    @Test
    fun getConfigUnit() {
        val config = Config.parseConfigFile(File("/tmp/config.conf"))
        val configUnit = config.getConfigUnit("www.baidu.com", 10)
        assertEquals(10, configUnit?.port)
    }
}