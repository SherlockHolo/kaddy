package kaddy.kio

import kotlinx.coroutines.experimental.nio.aAccept
import kotlinx.coroutines.experimental.runBlocking
import java.net.InetAddress
import java.net.InetSocketAddress
import java.nio.channels.AsynchronousServerSocketChannel

fun main(args: Array<String>) = runBlocking<Unit> {
    val server = AsynchronousServerSocketChannel.open()
    server.bind(InetSocketAddress(9876))

    val conn = server.aAccept()

    val writeBuffer = WriteBuffer(conn)
    writeBuffer.writeLine("sherlock")
    conn.close()
    writeBuffer.write(InetAddress.getByName("127.0.0.1").address)
}
