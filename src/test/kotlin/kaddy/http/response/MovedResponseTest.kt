package kaddy.http.response

import kaddy.http.Request
import kaddy.http.ResponseStatus
import kaddy.kio.ReadBuffer
import kotlinx.coroutines.experimental.nio.aAccept
import kotlinx.coroutines.experimental.runBlocking
import java.net.InetSocketAddress
import java.nio.channels.AsynchronousServerSocketChannel

fun main(args: Array<String>) = runBlocking {
    val server = AsynchronousServerSocketChannel.open().bind(InetSocketAddress(9876))
    while (true) {
        try {
            val conn = server.aAccept()
            Request.buildRequest(ReadBuffer(conn))
            val response = MovedResponse(conn, ResponseStatus.MOVED_PERMANENTLY, "http://www.qq.com")
            response.write()
        } finally {
        }
    }
}