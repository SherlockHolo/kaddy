package kaddy.http.response

import kaddy.http.Request
import kaddy.kio.ReadBuffer
import kotlinx.coroutines.experimental.async
import kotlinx.coroutines.experimental.nio.aAccept
import kotlinx.coroutines.experimental.runBlocking
import java.net.InetSocketAddress
import java.nio.channels.AsynchronousServerSocketChannel

fun main(args: Array<String>) = runBlocking {
    val server = AsynchronousServerSocketChannel.open().bind(InetSocketAddress(9876))

    while (true) {
        val socketChannel = server.aAccept()
        async {
            Request.buildRequest(ReadBuffer(socketChannel))
            InternalServerErrorResponse(socketChannel).write()
            socketChannel.close()
        }
    }
}